//
//  ExampleViewController.m
//  FCViewCellAdapterExample
//
//  Created by тест дядя тест on 20.11.13.
//
//

#import "ExampleViewController.h"
#import "FCViewCellAdapter.h"
#import "ImageCell.h"

#define CUSTOM_CELL_ID @"custom cell"
#define IMAGE_CELL_ID  @"image cell"



@interface ExampleViewController ()

@end

@implementation ExampleViewController

#pragma mark -FactoryViewCellDelegate

-(FCCellOptions *)options{
    FCCellOptions * opt = [FCCellOptions optionsWithNumberOfRowsBySection:@[@4, @3, @2]];
    [opt addOptionItemsInSection:0 withIdentifiers:@[CUSTOM_CELL_ID, CUSTOM_CELL_ID] andAliases:@[@"Cell1", @"Cell2"]];
    return opt;
}

-(NSString *)defaultIdentifier{
    return CUSTOM_CELL_ID;
}

-(NSNumber *)heightCellAll{
    return @(100);
}
-(void)setupCellBefore:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)path{
    //cell.textLabel.text = @"Before";
}

-(void)setupCellByAliasCell1:(UITableViewCell *)cell atPath:(NSIndexPath *)path{
    cell.textLabel.text = @"Cell1 Alias";
}

-(void)setupCellBySection0And3:(UITableViewCell *)cell{
    cell.textLabel.text = @"Cell with path 0, 3";
}

-(void)setupCellByIdCustomCell:(UITableViewCell *)cell atPath:(NSIndexPath *)path{
    //cell.textLabel.text = @"Custom cell";
}

-(void)setupCellBySection2:(UITableViewCell *)cell atPath:(NSIndexPath *)path{
    cell.textLabel.text = @"Section 2";
}

-(void)setupCellAfter:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)path{
    //cell.textLabel.text = @"After";
}


-(void)setupCellBySection0AtRangeWithLocation1AndLength5:(UITableViewCell *)cell atPath:(NSIndexPath *)path{
    cell.textLabel.text = @"By range 0, 1, 3";
}

#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
