//
//  FCViewController.h
//  FCViewCellAdapterExample
//
//  Created by тест дядя тест on 21.11.13.
//
//

#import <UIKit/UIKit.h>
#import "FCViewCellAdapter.h"
#import "FCRowAtIndexPathHandler.h"
#import "FCSelectRowAtIndexPathHandler.h"
#import "FCHeightForRowAtIndexPathHandler.h"

@interface FCViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, FCViewCellAdapterDelegate, FCHandlerDelegate, FCRowAtIndexPathHandlerDelegate, FCSelectRowAtIndexPathHandlerDelegate, FCHeightForRowAtIndexPathHandlerDelegate>
@end
