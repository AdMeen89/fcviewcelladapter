//
//  FactoryViewCell.h
//  WorkShop
//
//  Created by тест дядя тест on 20.11.13.
//  Copyright (c) 2013 тест дядя тест. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FCHandler.h"

@protocol FCViewCellAdapterDelegate <NSObject>
-(NSArray *)adapterHandlers;
@end

@interface FCViewCellAdapter: NSObject
@property(nonatomic, strong, setter = setDelegate:)id <FCViewCellAdapterDelegate> delegate;
@property(nonatomic, strong, setter = setHandlersDelegate:)id <FCHandlerDelegate> handlersDelegate;

-(void)getOptions;
-(NSInteger)sectionCount;
-(NSInteger)numberOfRowsInSection:(NSInteger)section;
-(id)bridgeForClass:(Class)className withParams:(NSDictionary *)params;

@end
