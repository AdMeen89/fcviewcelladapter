//
//  FactoryViewCell.m
//  WorkShop
//
//  Created by тест дядя тест on 20.11.13.
//  Copyright (c) 2013 тест дядя тест. All rights reserved.
//

#import "FCViewCellAdapter.h"

@interface FCViewCellAdapter ()
@property(nonatomic, strong)FCCellOptions * options;
@property(nonatomic, strong)NSMutableArray * handlers;
@end

@implementation FCViewCellAdapter

#pragma mark -
#pragma mark - init

-(FCViewCellAdapter *)init{
    self = [super init];
    if(self){
        _handlers = [NSMutableArray array];
    }
    return self;
}

-(NSInteger)sectionCount{
    return [_options sectionCount];
}

-(NSInteger)numberOfRowsInSection:(NSInteger)section{
    return [_options numberOfRowsInSection:section];
}

#pragma mark -work with handlers
-(void)addHandler:(Class)handler{
    id handlerRef = [[handler alloc] init];
    [_handlers addObject:handlerRef];
}

-(FCHandler *)findHandlerAtClass:(Class)classRef{
    for (FCHandler * handlerRef in _handlers) {
        if ([handlerRef isKindOfClass:classRef]) {
            return handlerRef;
        }
    }
    return nil;
}

#pragma mark -bridges

-(id)bridgeForClass:(Class)classRef withParams:(NSDictionary *)params{
    FCHandler * handler = [self findHandlerAtClass:classRef];
    return [handler bridgeWithParams:params];
}



#pragma mark -
#pragma mark - helpers

-(void)getOptions{
    self.options = [self.handlersDelegate options];
    for (FCHandler * handlerRef in _handlers) {
        handlerRef.options = self.options;
        [handlerRef getOptions];
    }
}


#pragma mark - set delegate

-(void)setHandlersDelegate:(id<FCHandlerDelegate>)handlersDelegate{
    NSArray * handlers = [self.delegate adapterHandlers];
    
    for (Class handlerClass in handlers) {
        [self addHandler:handlerClass];
    }
    
    _handlersDelegate = handlersDelegate;
    for (FCHandler * handlerRef in _handlers) {
        handlerRef.delegate = _handlersDelegate;
    }
}

-(void)setDelegate:(id<FCViewCellAdapterDelegate>)delegate{
    _delegate = delegate;
}

@end
