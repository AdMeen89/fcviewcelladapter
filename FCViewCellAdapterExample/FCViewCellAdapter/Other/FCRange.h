//
//  FCRange.h
//  FCViewCellAdapterExample
//
//  Created by тест дядя тест on 24.11.13.
//
//

#import <Foundation/Foundation.h>

@interface FCRange : NSObject<NSCopying>
@property(nonatomic)NSInteger section;
@property(nonatomic)NSRange range;

-(FCRange *)initWithSection:(NSInteger)section andRange:(NSRange)range;
-(FCRange *)initWithSection:(NSInteger)section andLocation:(NSInteger)location andLength:(NSInteger)length;

+(FCRange *)rangeWithSection:(NSInteger)section andRange:(NSRange)range;
+(FCRange *)rangeWithSection:(NSInteger)section andLocation:(NSInteger)location andLength:(NSInteger)length;

-(BOOL)containsIndexPath:(NSIndexPath *)path;

 @end
