//
//  FCRange.m
//  FCViewCellAdapterExample
//
//  Created by тест дядя тест on 24.11.13.
//
//

#import "FCRange.h"

@implementation FCRange
-(FCRange *)initWithSection:(NSInteger)section andRange:(NSRange)range{
    self = [super init];
    if(self){
        _section = section;
        _range   = range;
    }
    return self;
}

-(FCRange *)initWithSection:(NSInteger)section andLocation:(NSInteger)location andLength:(NSInteger)length{
    return [self initWithSection:section andRange:NSMakeRange(location, length)];
}

+(FCRange *)rangeWithSection:(NSInteger)section andRange:(NSRange)range{
    return [[FCRange alloc] initWithSection:section andRange:range];
}

+(FCRange *)rangeWithSection:(NSInteger)section andLocation:(NSInteger)location andLength:(NSInteger)length{
    return [[FCRange alloc] initWithSection:section andLocation:location andLength:length];
}

-(BOOL)containsIndexPath:(NSIndexPath *)path{
    if (_section != path.section) {
        return NO;
    }
    
    if ((path.item >= _range.location) && (path.item < (_range.location + _range.length))) {
        return YES;
    }
    return NO;
}


- (id)copyWithZone:(NSZone *)zone{
    return self;
}
@end