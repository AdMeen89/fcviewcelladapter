//
//  FCViewController.m
//  FCViewCellAdapterExample
//
//  Created by тест дядя тест on 21.11.13.
//
//

#import "FCViewController.h"



@interface FCViewController ()
@property(nonatomic, strong)FCViewCellAdapter * cellAdapter;
@end

@implementation FCViewController


#pragma mark - view adapter delegate
//подключим обработчики
-(NSArray *)adapterHandlers{
    return @[[FCRowAtIndexPathHandler class], [FCSelectRowAtIndexPathHandler class], [FCHeightForRowAtIndexPathHandler class]];
}

-(FCCellOptions *)options{
    return nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.cellAdapter = [[FCViewCellAdapter alloc] init];
    self.cellAdapter.delegate = self;
    self.cellAdapter.handlersDelegate = self;
    [self.cellAdapter getOptions];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(NSString *)defaultIdentifier{
    return nil;
}

#pragma mark -table delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.cellAdapter bridgeForClass:[FCSelectRowAtIndexPathHandler class] withParams:@{FC_PARAM_TABLE_VIEW:tableView, FC_PARAM_PATH:indexPath}];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSNumber * num = [self.cellAdapter bridgeForClass:[FCHeightForRowAtIndexPathHandler class] withParams:@{FC_PARAM_TABLE_VIEW:tableView, FC_PARAM_PATH:indexPath}];
    return [num floatValue];
}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    
//}

#pragma mark -table data source delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.cellAdapter sectionCount];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.cellAdapter numberOfRowsInSection:section];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self.cellAdapter bridgeForClass:[FCRowAtIndexPathHandler class] withParams:@{FC_PARAM_TABLE_VIEW:tableView, FC_PARAM_PATH:indexPath}];
}


@end
