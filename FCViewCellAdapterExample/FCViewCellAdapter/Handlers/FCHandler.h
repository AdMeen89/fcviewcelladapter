//
//  FCHandler.h
//  FCViewCellAdapterExample
//
//  Created by тест дядя тест on 21.11.13.
//
//

#import <Foundation/Foundation.h>
#import "FCCellOptionItem.h"
#import "FCCellOptions.h"

#define FC_PARAM_TABLE_VIEW @"tableView"
#define FC_PARAM_PATH       @"path"


@protocol FCHandlerDelegate <NSObject>

@required

-(FCCellOptions *)options;
-(NSString *)defaultIdentifier;
@end

@interface FCHandler : NSObject

@property(nonatomic, strong)id<FCHandlerDelegate> delegate;
@property(nonatomic, strong, setter = setOptions:)FCCellOptions * options;

-(void)getOptions;
-(id)bridgeWithParams:(NSDictionary *)params;
@end

@interface FCHandler(Protected)

-(NSString *)correctionOfName:(NSString *)name;
-(void)callMethodWithName:(NSString *)methodName andObject:(id)object1 andObject:(id)object2;
-(void)callMethodWithName:(NSString *)methodName andObject:(id)object;
-(void)callMethodWithName:(NSString *)methodName;

-(id)invokeMethodWithName:(NSString *)methodName;
-(id)invokeMethodWithName:(NSString *)methodName andObject:(id)object1 andObject:(id)object2;
-(id)invokeMethodWithName:(NSString *)methodName andObject:(id)object;

-(NSString *)methodNameByPath:(NSIndexPath *)path;


-(NSString *)rangePattern;
-(void)param:(id)param validateWithClass:(Class)className;
@end
