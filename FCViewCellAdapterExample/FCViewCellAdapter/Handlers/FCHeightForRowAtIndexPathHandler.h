//
//  www.h
//  FCViewCellAdapterExample
//
//  Created by тест дядя тест on 27.11.13.
//
//

#import "FCHandler.h"

#define FC_CELL_HEIGHT_BEFORE                        @"heightCellAll"

#define FC_CELL_HEIGHT_BY_ALIAS                      @"heightCellByAlias%@"
#define FC_CELL_HEIGHT_BY_PATH                       @"heightCellBySection%uAnd%u"
#define FC_CELL_HEIGHT_BY_ID                         @"heightCellById%@"
#define FC_CELL_HEIGHT_BY_SECTION                    @"heightCellBySection%u"

#define FC_CELL_HEIGHT_BY_SECTION_AND_RANGE          @"heightCellBySection%uAtRangeWithLocation%uAndLength%u"
#define FC_CELL_HEIGHT_BY_SECTION_AND_RANGE_PATTERN  @"heightCellBySection(\\d+)AtRangeWithLocation(\\d+)AndLength(\\d+)"

@protocol  FCHeightForRowAtIndexPathHandlerDelegate <NSObject>
@optional

-(NSNumber *)heightCellAll;
-(NSNumber *)heightCellByAlias$ALIASNAME$;
-(NSNumber *)heightCellBySection$SECTIONNUMBER$And$ITEMNUMBER$;
-(NSNumber *)heightCellById$ID$;
-(NSNumber *)heightCellBySection$SECTIONNUMBER$;
-(NSNumber *)heightCellBySection$SECTIONNUMBER$AtRangeWithLocation$LOCATION$AndLength$LENGTH$;

@end

@interface FCHeightForRowAtIndexPathHandler : FCHandler

@end
