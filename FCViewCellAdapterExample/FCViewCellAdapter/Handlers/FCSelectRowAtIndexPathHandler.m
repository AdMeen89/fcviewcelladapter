//
//  FCdiDSelect.m
//  FCViewCellAdapterExample
//
//  Created by тест дядя тест on 21.11.13.
//
//

#import "FCSelectRowAtIndexPathHandler.h"


@implementation FCSelectRowAtIndexPathHandler

#pragma mark - entry point

-(id)bridgeWithParams:(NSDictionary *)params{
    id tableView = [params objectForKey:FC_PARAM_TABLE_VIEW];
    [self param:tableView validateWithClass:[UITableView class]];
    
    id path = [params objectForKey:FC_PARAM_PATH];
    [self param:path validateWithClass:[NSIndexPath class]];
    
    [self tableView:tableView didSelectRowAtIndexPath:path];
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)path{
    FCCellOptionItem * currentItem = [self.options optionItemWithIndexPath:path];
    [self invokeCellSelectedBeforeWithPath:path];
    
    [self invokeCellSelectedBySection:path.section atPath:path];
    
    NSString * ID = [self.delegate defaultIdentifier];
    
    if (currentItem.identifier) {
        ID = currentItem.identifier;
    }
    
    [self invokeCellSelectedById:ID atPath:path];
    
    [self invokeCellSelectedByRangeWithPath:path];
    
    
    if (currentItem.alias) {
        [self invokeCellSelectedByAlias:currentItem.alias atPath:path];
    }
    
    [self invokeCellSelectedByPath:path];
    
    [self invokeCellSelectedAfterWithPath:path];
        
}

#pragma mark - pattern for range

-(NSString *)rangePattern{
    return FC_CELL_SELECTED_BY_SECTION_AND_RANGE_PATTERN;
}


#pragma mark - invoke methods

-(void)invokeCellSelectedBeforeWithPath:(NSIndexPath *)path{
    [self callMethodWithName:FC_CELL_SELECTED_BEFORE andObject:path];
}

-(void)invokeCellSelectedBySection:(NSInteger)section atPath:(NSIndexPath *)path{
    NSString * nameMethod = [NSString stringWithFormat:FC_CELL_SELECTED_BY_SECTION, section];
    [self callMethodWithName:nameMethod andObject:path];
}

-(void)invokeCellSelectedById:(NSString *)ID atPath:(NSIndexPath *)path{
    NSString * clearId = [self correctionOfName:ID];
    NSString * nameMethod = [NSString stringWithFormat:FC_CELL_SELECTED_BY_ID, clearId];
    [self callMethodWithName:nameMethod andObject:path];
}

-(void)invokeCellSelectedByAlias:(NSString *)alias atPath:(NSIndexPath *)path{
    NSString * nameMethod = [NSString stringWithFormat:FC_CELL_SELECTED_BY_ALIAS, alias];
    [self callMethodWithName:nameMethod andObject:path];
}

-(void)invokeCellSelectedByPath:(NSIndexPath *)path{
    NSString * nameMethod = [NSString stringWithFormat:FC_CELL_SELECTED_BY_PATH, path.section, path.item];
    [self callMethodWithName:nameMethod];
}

-(void)invokeCellSelectedAfterWithPath:(NSIndexPath *)path{
    [self callMethodWithName:FC_CELL_SELECTED_AFTER andObject:path];
}

-(void)invokeCellSelectedByRangeWithPath:(NSIndexPath *)path{
    NSString * methodName = [self methodNameByPath:path];
    if(!methodName){
        return;
    }
    [self callMethodWithName:methodName andObject:path];
}

@end
