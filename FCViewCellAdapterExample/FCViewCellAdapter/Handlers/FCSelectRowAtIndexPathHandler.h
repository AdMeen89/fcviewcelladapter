//
//  ha.h
//  FCViewCellAdapterExample
//
//  Created by тест дядя тест on 22.11.13.
//
//

#import "FCHandler.h"

#define FC_CELL_SELECTED_BEFORE                        @"selectedCellBefore:forRowAtIndexPath:"
#define FC_CELL_SELECTED_AFTER                         @"selectedCellAfter:forRowAtIndexPath:"

#define FC_CELL_SELECTED_BY_ALIAS                      @"selectedCellByAlias%@:atPath:"
#define FC_CELL_SELECTED_BY_PATH                       @"selectedCellBySection%uAnd%u:"
#define FC_CELL_SELECTED_BY_ID                         @"selectedCellById%@:atPath:"
#define FC_CELL_SELECTED_BY_SECTION                    @"selectedCellBySection%u:atPath:"

#define FC_CELL_SELECTED_BY_SECTION_AND_RANGE          @"selectedCellBySection%uAtRangeWithLocation%uAndLength%u:atPath:"
#define FC_CELL_SELECTED_BY_SECTION_AND_RANGE_PATTERN  @"selectedCellBySection(\\d+)AtRangeWithLocation(\\d+)AndLength(\\d+):atPath:"

@protocol  FCSelectRowAtIndexPathHandlerDelegate <NSObject>
@optional

-(void)selectedCellBefore:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)path;
-(void)selectedCellAfter:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)path;
-(void)selectedCellByAlias$ALIASNAME$:(UITableViewCell *)cell atPath:(NSIndexPath *)path;
-(void)selectedCellBySection$SECTIONNUMBER$And$ITEMNUMBER$:(UITableViewCell *)cell;
-(void)selectedCellById$ID$:(UITableViewCell *)cell atPath:(NSIndexPath *)path;
-(void)selectedCellBySection$SECTIONNUMBER$:(UITableViewCell *)cell atPath:(NSIndexPath *)path;
-(void)selectedCellBySection$SECTIONNUMBER$AtRangeWithLocation$LOCATION$AndLength$LENGTH$:(UITableViewCell *)cell atPath:(NSIndexPath *)path;

@end

@interface FCSelectRowAtIndexPathHandler : FCHandler
@end
