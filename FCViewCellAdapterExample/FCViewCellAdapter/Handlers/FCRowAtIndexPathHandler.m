//
//  FCdiDSelect.m
//  FCViewCellAdapterExample
//
//  Created by тест дядя тест on 21.11.13.
//
//

#import "FCRowAtIndexPathHandler.h"

@interface FCRowAtIndexPathHandler()
@property(nonatomic, strong)UITableViewCell * cell;
@end

@implementation FCRowAtIndexPathHandler

#pragma mark - entry point
-(id)bridgeWithParams:(NSDictionary *)params{
    id tableView = [params objectForKey:FC_PARAM_TABLE_VIEW];
    [self param:tableView validateWithClass:[UITableView class]];
    
    id path = [params objectForKey:FC_PARAM_PATH];
    [self param:path validateWithClass:[NSIndexPath class]];
    
    return [self tableView:tableView cellForRowAtIndexPath:path];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)path{
    FCCellOptionItem * currentItem = [self.options optionItemWithIndexPath:path];
    
    if(!currentItem.identifier){
        self.cell =  [tableView dequeueReusableCellWithIdentifier:[self.delegate defaultIdentifier] forIndexPath:path];
    }else{
        self.cell = [tableView dequeueReusableCellWithIdentifier:currentItem.identifier forIndexPath:path];
    }
    [self invokeCellSetupBeforeWithPath:path];

    [self invokeCellSetupBySection:path.section atPath:path];
    
    NSString * ID = [self.delegate defaultIdentifier];
    
    if (currentItem.identifier) {
        ID = currentItem.identifier;
    }
    
    [self invokeCellSetupById:ID atPath:path];
    
    [self invokeCellSetupByRangeWithPath:path];
    
    
    if (currentItem.alias) {
        [self invokeCellSetupByAlias:currentItem.alias atPath:path];
    }
    
    [self invokeCellSetupByPath:path];
    
    
    
    [self invokeCellSetupAfterWithPath:path];
    
    
    return self.cell;
}

#pragma mark - pattern for range

-(NSString *)rangePattern{
    return FC_CELL_SETUP_BY_SECTION_AND_RANGE_PATTERN;
}

#pragma mark - invoke methods

-(void)invokeCellSetupBeforeWithPath:(NSIndexPath *)path{
    [self callMethodWithName:FC_CELL_SETUP_BEFORE andObject:self.cell andObject:path];
}

-(void)invokeCellSetupBySection:(NSInteger)section atPath:(NSIndexPath *)path{
    NSString * nameMethod = [NSString stringWithFormat:FC_CELL_SETUP_BY_SECTION, section];
    [self callMethodWithName:nameMethod andObject:self.cell andObject:path];
}

-(void)invokeCellSetupById:(NSString *)ID atPath:(NSIndexPath *)path{
    NSString * clearId = [self correctionOfName:ID];
    NSString * nameMethod = [NSString stringWithFormat:FC_CELL_SETUP_BY_ID, clearId];
    [self callMethodWithName:nameMethod andObject:self.cell andObject:path];
}

-(void)invokeCellSetupByAlias:(NSString *)alias atPath:(NSIndexPath *)path{
    NSString * nameMethod = [NSString stringWithFormat:FC_CELL_SETUP_BY_ALIAS, alias];
    [self callMethodWithName:nameMethod andObject:self.cell andObject:path];
}

-(void)invokeCellSetupByPath:(NSIndexPath *)path{
    NSString * nameMethod = [NSString stringWithFormat:FC_CELL_SETUP_BY_PATH, path.section, path.item];
    [self callMethodWithName:nameMethod andObject:self.cell];
}

-(void)invokeCellSetupAfterWithPath:(NSIndexPath *)path{
     [self callMethodWithName:FC_CELL_SETUP_AFTER andObject:self.cell andObject:path];
}

-(void)invokeCellSetupByRangeWithPath:(NSIndexPath *)path{
    NSString * methodName = [self methodNameByPath:path];
    if(!methodName){
        return;
    }
    [self callMethodWithName:methodName andObject:self.cell andObject:path];
}

@end
