//
//  FCdiDSelect.h
//  FCViewCellAdapterExample
//
//  Created by тест дядя тест on 21.11.13.
//
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
#import "FCHandler.h"

#define FC_CELL_SETUP_BEFORE                        @"setupCellBefore:forRowAtIndexPath:"
#define FC_CELL_SETUP_AFTER                         @"setupCellAfter:forRowAtIndexPath:"

#define FC_CELL_SETUP_BY_ALIAS                      @"setupCellByAlias%@:atPath:"
#define FC_CELL_SETUP_BY_PATH                       @"setupCellBySection%uAnd%u:"
#define FC_CELL_SETUP_BY_ID                         @"setupCellById%@:atPath:"
#define FC_CELL_SETUP_BY_SECTION                    @"setupCellBySection%u:atPath:"

#define FC_CELL_SETUP_FIRST                         @"setupCellFirst:forRowAtIndexPath:"        //todo
#define FC_CELL_SETUP_LAST                          @"setupCellLast:forRowAtIndexPath:"         //todo

#define FC_CELL_SETUP_FIRST_IN_SECTION              @"setupCellFirstInSection:forRowAtIndexPath:"   //todo
#define FC_CELL_SETUP_LAST_IN_SECTION               @"setupCellFirstInSection:forRowAtIndexPath:"   //todo

#define FC_CELL_SETUP_ODD                           @"setupCellOdd:forRowAtIndexPath:"              //todo
#define FC_CELL_SETUP_EVEN                          @"setupCellEven:forRowAtIndexPath:"             //todo

#define FC_CELL_SETUP_BY_SECTION_AND_RANGE          @"setupCellBySection%uAtRangeWithLocation%uAndLength%u:atPath:"
#define FC_CELL_SETUP_BY_SECTION_AND_RANGE_PATTERN  @"setupCellBySection(\\d+)AtRangeWithLocation(\\d+)AndLength(\\d+):atPath:"

@protocol  FCRowAtIndexPathHandlerDelegate <NSObject>
@optional

-(void)setupCellBefore:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)path;
-(void)setupCellAfter:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)path;
-(void)setupCellByAlias$ALIASNAME$:(UITableViewCell *)cell atPath:(NSIndexPath *)path;
-(void)setupCellBySection$SECTIONNUMBER$And$ITEMNUMBER$:(UITableViewCell *)cell;
-(void)setupCellById$ID$:(UITableViewCell *)cell atPath:(NSIndexPath *)path;
-(void)setupCellBySection$SECTIONNUMBER$:(UITableViewCell *)cell atPath:(NSIndexPath *)path;
-(void)setupCellFirst:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)path;
-(void)setupCellLast:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)path;
-(void)setupCellFirstInSection:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)path;
-(void)setupCellLastInSection:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)path;
-(void)setupCellBySection$SECTIONNUMBER$AtRangeWithLocation$LOCATION$AndLength$LENGTH$:(UITableViewCell *)cell atPath:(NSIndexPath *)path;
@end

@interface FCRowAtIndexPathHandler : FCHandler
@end
