//
//  FCHandler.m
//  FCViewCellAdapterExample
//
//  Created by тест дядя тест on 21.11.13.
//
//

#import "FCHandler.h"
#import <objc/runtime.h>
#import "FCRange.h"

@interface FCHandler()
@property(nonatomic, strong)NSMutableArray * delegateMethodsList;
@property(nonatomic, strong)NSMutableDictionary * rangesMethods;
@end

@implementation FCHandler



#pragma mark - public

-(void)setOptions:(FCCellOptions *)options{
    _options = options;
    [self getDelegateMethods];
    [self allRange];
}

-(void)getOptions{
    
}

#pragma mark - bridge

-(id)bridgeWithParams:(NSDictionary *)params{
    return nil;
}

#pragma mark - protection

-(NSString *)correctionOfName:(NSString *)name{
    name = [name capitalizedString];
    return [name stringByReplacingOccurrencesOfString:@" " withString:@""];
}

#pragma mark - call methods methods

-(void)callMethodWithName:(NSString *)methodName andObject:(id)object1 andObject:(id)object2{
    SEL sel = NSSelectorFromString(methodName);
    if([self.delegate respondsToSelector:sel]){
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.delegate performSelector:sel withObject:object1 withObject:object2];
        #pragma clang diagnostic pop
    }
}

-(void)callMethodWithName:(NSString *)methodName andObject:(id)object{
    SEL sel = NSSelectorFromString(methodName);
    if([self.delegate respondsToSelector:sel]){
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.delegate performSelector:sel withObject:object];
        #pragma clang diagnostic pop
    }
}

-(void)callMethodWithName:(NSString *)methodName{
    SEL sel = NSSelectorFromString(methodName);
    if([self.delegate respondsToSelector:sel]){
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.delegate performSelector:sel];
        #pragma clang diagnostic pop
    }
}

-(id)invokeMethodWithName:(NSString *)methodName{
    SEL sel = NSSelectorFromString(methodName);
    if([self.delegate respondsToSelector:sel]){
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        return [self.delegate performSelector:sel];
        #pragma clang diagnostic pop
    }
    return nil;
}

-(id)invokeMethodWithName:(NSString *)methodName andObject:(id)object1 andObject:(id)object2{
    SEL sel = NSSelectorFromString(methodName);
    if([self.delegate respondsToSelector:sel]){
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        return [self.delegate performSelector:sel withObject:object1 withObject:object2];
        #pragma clang diagnostic pop
    }
    return nil;
}

-(id)invokeMethodWithName:(NSString *)methodName andObject:(id)object{
    SEL sel = NSSelectorFromString(methodName);
    if([self.delegate respondsToSelector:sel]){
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        return [self.delegate performSelector:sel withObject:object];
        #pragma clang diagnostic pop
    }
    return nil;
}

#pragma mark - methodList

-(void)getDelegateMethods{
    Class clazz = [self.delegate class];
    u_int count;
    
    Method* methods = class_copyMethodList(clazz, &count);
    self.delegateMethodsList = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count ; i++)
    {
        SEL selector = method_getName(methods[i]);
        const char* methodName = sel_getName(selector);
        [self.delegateMethodsList addObject:[NSString  stringWithCString:methodName encoding:NSUTF8StringEncoding]];
    }
    free(methods);
}


-(void)allRange{
    _rangesMethods = [NSMutableDictionary dictionary];
    NSError * error = nil;
    NSRegularExpression * regExp = [NSRegularExpression regularExpressionWithPattern:[self rangePattern] options:0 error:&error];
    NSRegularExpression * numRegExp = [NSRegularExpression regularExpressionWithPattern:@"(\\d+)" options:0 error:&error];
    for (NSString * methodName in _delegateMethodsList) {
        NSTextCheckingResult *match = [regExp firstMatchInString:methodName options:0 range:NSMakeRange(0, [methodName length])];
        if (match) {
            NSArray *nMatch = [numRegExp matchesInString:methodName options:0 range:NSMakeRange(0, [methodName length])];
            NSRange range;
            range = [[nMatch objectAtIndex:0] range];
            NSInteger section = [[methodName substringWithRange:range] integerValue];
            
            range = [[nMatch objectAtIndex:1] range];
            NSInteger  location = [[methodName substringWithRange:range] integerValue];
            
            range = [[nMatch objectAtIndex:2] range];
            NSInteger lenght = [[methodName substringWithRange:range] integerValue];
            
            [_rangesMethods setObject:methodName forKey:[FCRange rangeWithSection:section andLocation:location andLength:lenght]];
        };
    }
}

-(NSString *)methodNameByPath:(NSIndexPath *)path{
    NSArray * keys = [_rangesMethods allKeys];
    for (FCRange * range in keys) {
        if ([range containsIndexPath:path]) {
            return [_rangesMethods objectForKey:range];
        }
    }
    return nil;
}

-(void)param:(id)param validateWithClass:(Class)className{
    if(![param isKindOfClass:className]){
        [NSException exceptionWithName:@"sssss" reason:@"" userInfo:nil];
    }
}

@end
