//
//  www.m
//  FCViewCellAdapterExample
//
//  Created by тест дядя тест on 27.11.13.
//
//

#import "FCHeightForRowAtIndexPathHandler.h"

@interface FCHeightForRowAtIndexPathHandler()
@property(nonatomic, strong)NSNumber * height;
@end

@implementation FCHeightForRowAtIndexPathHandler

#pragma mark - options
-(void)setOptions:(FCCellOptions *)options{
    [super setOptions:options];
    _height = [NSNumber numberWithFloat:0.0];
}

#pragma mark - pattern for range

-(NSString *)rangePattern{
    return FC_CELL_HEIGHT_BY_SECTION_AND_RANGE_PATTERN;
}

#pragma mark - entry point

-(id)bridgeWithParams:(NSDictionary *)params{
    id tableView = [params objectForKey:FC_PARAM_TABLE_VIEW];
    [self param:tableView validateWithClass:[UITableView class]];
    
    id path = [params objectForKey:FC_PARAM_PATH];
    [self param:path validateWithClass:[NSIndexPath class]];
    
    return [self tableView:tableView heightForRowAtIndexPath:path];
}

- (NSNumber *)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)path{
    
    FCCellOptionItem * currentItem = [self.options optionItemWithIndexPath:path];
    [self invokeCellHeightAll];
    
    [self invokeCellHeightBySection:path.section];
    
    NSString * ID = [self.delegate defaultIdentifier];
    
    if (currentItem.identifier) {
        ID = currentItem.identifier;
    }
    
    [self invokeCellHeightById:ID];
    
    [self invokeCellHeightByRangeWithPath:path];
    
    
    if (currentItem.alias) {
        [self invokeCellHeightByAlias:currentItem.alias];
    }
    
    [self invokeCellHeightByPath:path];
    return _height;
}


#pragma mark - invoke methods
#pragma mark - for all

-(void)invokeCellHeightAll{
    _height = [self invokeMethodWithName:FC_CELL_HEIGHT_BEFORE];
}


#pragma mark - other

-(void)invokeCellHeightBySection:(NSInteger)section{
    NSString * nameMethod = [NSString stringWithFormat:FC_CELL_HEIGHT_BY_SECTION, section];
    NSNumber * retRef = [self invokeMethodWithName:nameMethod];
    _height = retRef ? retRef : _height;
}

-(void)invokeCellHeightById:(NSString *)ID{
    NSString * clearId = [self correctionOfName:ID];
    NSString * nameMethod = [NSString stringWithFormat:FC_CELL_HEIGHT_BY_ID, clearId];
    NSNumber * retRef = [self invokeMethodWithName:nameMethod];
    _height = retRef ? retRef : _height;
}

-(void)invokeCellHeightByAlias:(NSString *)alias{
    NSString * nameMethod = [NSString stringWithFormat:FC_CELL_HEIGHT_BY_ALIAS, alias];
    NSNumber * retRef = [self invokeMethodWithName:nameMethod];
    _height = retRef ? retRef : _height;
}

-(void)invokeCellHeightByPath:(NSIndexPath *)path{
    NSString * nameMethod = [NSString stringWithFormat:FC_CELL_HEIGHT_BY_PATH, path.section, path.item];
    NSNumber * retRef = [self invokeMethodWithName:nameMethod];
    _height = retRef ? retRef : _height;
}

#pragma mark - range


-(void)invokeCellHeightByRangeWithPath:(NSIndexPath *)path{
    NSString * nameMethod = [self methodNameByPath:path];
    if(!nameMethod){
        return;
    }
    NSNumber * retRef = [self invokeMethodWithName:nameMethod];
    _height = retRef ? retRef : _height;
}

@end
