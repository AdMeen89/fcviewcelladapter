//
//  FCCellOptionItem.m
//  WorkShop
//
//  Created by тест дядя тест on 20.11.13.
//  Copyright (c) 2013 тест дядя тест. All rights reserved.
//

#import "FCCellOptionItem.h"


@implementation FCCellOptionItem
-(FCCellOptionItem *)initWithCellIdentifier:(NSString *) identifier andAlias:(NSString *)alias andIndexPath:(NSIndexPath *)indexPath{
    self = [super init];
    if(self){        
        self.identifier = identifier;
        self.alias = alias;
        self.indexPath = indexPath;
    }
    return self;
}

+(FCCellOptionItem *)itemWithCellIdentifier:(NSString *)identifier andAlias:(NSString *)alias andIndexPath:(NSIndexPath *)indexPath{
    return [[FCCellOptionItem alloc] initWithCellIdentifier:identifier andAlias:alias andIndexPath:indexPath];
}

+(FCCellOptionItem *)itemWithPath:(NSIndexPath *)path{
    return [[FCCellOptionItem alloc] initWithCellIdentifier:nil andAlias:nil andIndexPath:path];
}
@end