//
//  FCCellOption.m
//  WorkShop
//
//  Created by тест дядя тест on 20.11.13.
//  Copyright (c) 2013 тест дядя тест. All rights reserved.
//

#import "FCCellOptions.h"


@interface FCCellOptions()
@property(nonatomic, strong)NSMutableArray * sections;
@end

#pragma mark - public
#pragma mark - initers

@implementation FCCellOptions

-(FCCellOptions *)initWithNumberOfRowsBySection:(NSArray *)rowsInSection{
    self = [super init];
    if(self){
        self.sections = [NSMutableArray array];
        [self setupSectionsWithNumberOfRowsBySection:rowsInSection];
    }
    return self;
}

+(FCCellOptions *)optionsWithNumberOfRowsBySection:(NSArray *)rowsInSection{
    return [[FCCellOptions alloc] initWithNumberOfRowsBySection:rowsInSection];
}

-(FCCellOptionItem *)optionItemWithIndexPath:(NSIndexPath *)indexPath{
    return [[self sectionDataWithSectionNumber:indexPath.section] objectForKey:indexPath];
}

-(void)updateOptionItem:(FCCellOptionItem *)item{
    if (![self optionItemWithIndexPath:item.indexPath]) {
        return;
    }
    
    [self insertOptionItem:item];
}

#pragma mark - add methods

-(void)addOptionItemsInSection:(NSInteger)section withIdentifiers:(NSArray *)identifiers andAliases:(NSArray *)aliases{
    for (int i = 0; i < [identifiers count]; i++) {
        [self addOptionItemInSection:section withAlias:[aliases objectAtIndex:i] andIdentifier:[identifiers objectAtIndex:i]];
    }
}

-(void)addOptionItemsInSection:(NSInteger)section withNumbers:(NSInteger)numbers;{
    for (int i = 0; i < numbers; i++) {
        [self addOptionItemInSection:section withAlias:nil andIdentifier:nil];
    }
}

-(void)addOptionItemInSection:(NSInteger)section withAlias:(NSString *)alias andIdentifier:(NSString *)identifier{
    
    if (section >= [self sectionCount]) {
        [NSException exceptionWithName:@"-(void)insertOptionItemInSection:(NSInteger)section" reason:@"out of bounds" userInfo:nil];
        return;
    }
    
    NSInteger maxItemPath = [self numberOfRowsInSection:section] + 1;
    NSIndexPath * path = [NSIndexPath indexPathForItem:maxItemPath inSection:section];
    [self insertOptionItem:[FCCellOptionItem itemWithCellIdentifier:identifier andAlias:alias andIndexPath:path]];
}

-(void)addOptionItemInSection:(NSInteger)section{
    [self addOptionItemInSection:section withAlias:nil andIdentifier:nil];
}


#pragma mark - private methods

-(void)setupSectionsWithNumberOfRowsBySection:(NSArray *)rowsInSection{
    for (NSNumber * numOfRows in rowsInSection) {
        NSInteger section = [rowsInSection indexOfObject:numOfRows];
        [self setupSection:section withWithNumberOfRows:[numOfRows integerValue]];
    }
}

-(void)setupSection:(NSInteger)section withWithNumberOfRows:(NSInteger)number{
    NSMutableDictionary * sectionData = [NSMutableDictionary dictionaryWithCapacity:number];
    [_sections addObject:sectionData];
    for (int  i = 0; i < number; i++) {
        NSIndexPath * path = [NSIndexPath indexPathForItem:i inSection:section];
        FCCellOptionItem * item = [FCCellOptionItem itemWithPath:path];
        [self insertOptionItem:item];
    }
}


-(void)insertOptionItem:(FCCellOptionItem *)item{
    if (item.indexPath.section > [self sectionCount]) {
        return;
    }

    [[self sectionDataWithSectionNumber:item.indexPath.section] setObject:item forKey:item.indexPath];

}


-(NSInteger)sectionCount{
    return [_sections count];
}

-(NSInteger)numberOfRowsInSection:(NSInteger)section{
    return [[_sections objectAtIndex:section] count];
}

-(NSMutableDictionary *)sectionDataWithSectionNumber:(NSInteger)section{
    return [_sections objectAtIndex:section];
}

@end
