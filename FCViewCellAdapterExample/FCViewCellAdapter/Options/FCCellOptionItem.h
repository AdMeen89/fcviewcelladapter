//
//  FCCellOptionItem.h
//  WorkShop
//
//  Created by тест дядя тест on 20.11.13.
//  Copyright (c) 2013 тест дядя тест. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FCCellOptionItem : NSObject
@property(nonatomic, strong)NSString * identifier;
@property(nonatomic, strong)NSString * alias;
@property(nonatomic, strong)NSIndexPath * indexPath;

-(FCCellOptionItem *)initWithCellIdentifier:(NSString *) identifier andAlias:(NSString *) alias andIndexPath:(NSIndexPath *)indexPath;
+(FCCellOptionItem *)itemWithCellIdentifier:(NSString *) identifier andAlias:(NSString *) alias andIndexPath:(NSIndexPath *)indexPath;

+(FCCellOptionItem *)itemWithPath:(NSIndexPath *)path;
@end
