//
//  FCCellOption.h
//  WorkShop
//
//  Created by тест дядя тест on 20.11.13.
//  Copyright (c) 2013 тест дядя тест. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FCCellOptionItem.h"

@interface FCCellOptions : NSObject

#pragma mark - initers

-(FCCellOptions *)initWithNumberOfRowsBySection:(NSArray *)rowsInSection;
+(FCCellOptions *)optionsWithNumberOfRowsBySection:(NSArray *)rowsInSection;

#pragma mark - select item
-(FCCellOptionItem *)optionItemWithIndexPath:(NSIndexPath *)indexPath;


#pragma mark - add items
-(void)addOptionItemsInSection:(NSInteger)section withNumbers:(NSInteger)numbers;
-(void)addOptionItemsInSection:(NSInteger)section withIdentifiers:(NSArray *)identifiers andAliases:(NSArray *)aliases;
-(void)addOptionItemInSection:(NSInteger)section;
-(void)addOptionItemInSection:(NSInteger)section withAlias:(NSString *)alias andIdentifier:(NSString *)identifier;

#pragma mark - helper

-(NSInteger)sectionCount;
-(NSInteger)numberOfRowsInSection:(NSInteger)section;
@end
