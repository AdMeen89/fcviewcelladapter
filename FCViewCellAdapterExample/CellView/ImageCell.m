//
//  ImageCell.m
//  FCViewCellAdapterExample
//
//  Created by тест дядя тест on 20.11.13.
//
//

#import "ImageCell.h"

@interface ImageCell ()
@property(nonatomic, weak)IBOutlet UIImageView * imageView;
@end

@implementation ImageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setImage{
    self.imageView.image = [UIImage imageNamed:@"skype-75"];
}

@end
