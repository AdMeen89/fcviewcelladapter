//
//  main.m
//  FCViewCellAdapterExample
//
//  Created by тест дядя тест on 20.11.13.
//
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
